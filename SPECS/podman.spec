%global with_debug 1
%global with_check 0

%if 0%{?with_debug}
%global _find_debuginfo_dwz_opts %{nil}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package   %{nil}
%endif

%define gobuild(o:) \
go build -buildmode pie -compiler gc -tags="rpm_crashtraceback no_openssl ${BUILDTAGS:-}" -ldflags "${LDFLAGS:-} -compressdwarf=false -B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' \\n') -extldflags '%__global_ldflags'" -a -v -x %{?**};
%define gogenerate go generate

%if 0%{?rhel} > 7 || 0%{?fedora}
%bcond_without varlink
%else
%bcond_with varlink
%endif

%global provider github
%global provider_tld com
%global project containers
%global repo libpod
# https://github.com/containers/libpod
%global provider_prefix %{provider}.%{provider_tld}/%{project}/%{repo}
%global import_path %{provider_prefix}
%global git_podman https://%{provider}.%{provider_tld}/%{project}/%{repo}
%global commit 921f98f8795eb9fcb19ce581020cfdeff6dee09f
%global shortcommit %(c=%{commit}; echo ${c:0:7})

%global import_path_conmon github.com/kubernetes-sigs/cri-o
%global git_conmon https://%{import_path_conmon}
%global commit_conmon 9b1f0a08285a7f74b21cc9b6bfd98a48905a7ba2
%global shortcommit_conmon %(c=%{commit_conmon}; echo ${c:0:7})

Name: podman
Version: 1.0.0
Release: 8.git%{shortcommit}%{?dist}
Summary: Manage Pods, Containers and Container Images
License: ASL 2.0
URL: %{git_podman}
Source0: %{git_podman}/archive/%{commit}/%{repo}-%{shortcommit}.tar.gz
Source1: %{git_conmon}/archive/%{commit_conmon}/cri-o-%{shortcommit_conmon}.tar.gz
# tracker bug: https://bugzilla.redhat.com/show_bug.cgi?id=CVE-2020-10696
# backported:  https://github.com/containers/buildah/commit/c61925b8936e93a5e900f91b653a846f7ea3a9ed.patch
Patch0: podman-CVE-2020-10696.patch
# related bug: https://bugzilla.redhat.com/show_bug.cgi?id=1882267
# patch:       https://github.com/mheon/libpod/commit/bc5be3ca10cd4c147955fadd2586b5dd8ad0eeea.patch
Patch1: podman-1882267.patch
# related bug: https://bugzilla.redhat.com/show_bug.cgi?id=1918285
Patch2: podman-CVE-2021-20188.patch
# related bug: https://bugzilla.redhat.com/show_bug.cgi?id=1930552
Patch3: 0001-Only-drop-all-caps-in-exec-when-non-root.patch

# e.g. el6 has ppc64 arch without gcc-go, so EA tag is required
#ExclusiveArch:  %%{?go_arches:%%{go_arches}}%%{!?go_arches:%%{ix86} x86_64 aarch64 %%{arm}}
ExclusiveArch: aarch64 %{arm} ppc64le s390x x86_64
# If go_compiler is not set to 1, there is no virtual provide. Use golang instead.
BuildRequires: %{?go_compiler:compiler(go-compiler)}%{!?go_compiler:golang}
BuildRequires: device-mapper-devel
BuildRequires: glib2-devel
BuildRequires: glibc-devel
BuildRequires: glibc-static
BuildRequires: git
BuildRequires: go-md2man
BuildRequires: gpgme-devel
BuildRequires: libassuan-devel
BuildRequires: libgpg-error-devel
BuildRequires: libseccomp-devel
BuildRequires: libselinux-devel
BuildRequires: ostree-devel
BuildRequires: pkgconfig
Requires: runc
Requires: containers-common >= 0.1.29-3
# can't use default conmon right now, so we ship our own
#Requires:       conmon
Requires: containernetworking-plugins >= 0.7.3-5
Requires: iptables
Requires: nftables
Requires: oci-systemd-hook
Recommends: container-selinux
Recommends: slirp4netns
Recommends: fuse-overlayfs

# vendored libraries
# awk '{print "Provides: bundled(golang("$1")) = "$2}' vendor.conf | sort
# [thanks to Carl George <carl@george.computer> for containerd.spec]
Provides: bundled(golang(github.com/Azure/go-ansiterm)) = 19f72df4d05d31cbe1c56bfc8045c96babff6c7e
Provides: bundled(golang(github.com/blang/semver)) = v3.5.0
Provides: bundled(golang(github.com/boltdb/bolt)) = master
Provides: bundled(golang(github.com/buger/goterm)) = 2f8dfbc7dbbff5dd1d391ed91482c24df243b2d3
Provides: bundled(golang(github.com/BurntSushi/toml)) = v0.2.0
Provides: bundled(golang(github.com/containerd/cgroups)) = 77e628511d924b13a77cebdc73b757a47f6d751b
Provides: bundled(golang(github.com/containerd/continuity)) = master
Provides: bundled(golang(github.com/containernetworking/cni)) = v0.7.0-alpha1
Provides: bundled(golang(github.com/containernetworking/plugins)) = 1562a1e60ed101aacc5e08ed9dbeba8e9f3d4ec1
Provides: bundled(golang(github.com/containers/image)) = 134f99bed228d6297dc01d152804f6f09f185418
Provides: bundled(golang(github.com/containers/psgo)) = 382fc951fe0a8aba62043862ce1a56f77524db87
Provides: bundled(golang(github.com/containers/storage)) = 17c7d1fee5603ccf6dd97edc14162fc1510e7e23
Provides: bundled(golang(github.com/coreos/go-systemd)) = v14
Provides: bundled(golang(github.com/cri-o/ocicni)) = master
Provides: bundled(golang(github.com/cyphar/filepath-securejoin)) = v0.2.1
Provides: bundled(golang(github.com/davecgh/go-spew)) = v1.1.0
Provides: bundled(golang(github.com/docker/distribution)) = 7a8efe719e55bbfaff7bc5718cdf0ed51ca821df
Provides: bundled(golang(github.com/docker/docker)) = 86f080cff0914e9694068ed78d503701667c4c00
Provides: bundled(golang(github.com/docker/docker-credential-helpers)) = d68f9aeca33f5fd3f08eeae5e9d175edf4e731d1
Provides: bundled(golang(github.com/docker/go-connections)) = 3ede32e2033de7505e6500d6c868c2b9ed9f169d
Provides: bundled(golang(github.com/docker/go-units)) = v0.3.2
Provides: bundled(golang(github.com/docker/libtrust)) = aabc10ec26b754e797f9028f4589c5b7bd90dc20
Provides: bundled(golang(github.com/docker/spdystream)) = ed496381df8283605c435b86d4fdd6f4f20b8c6e
Provides: bundled(golang(github.com/fatih/camelcase)) = f6a740d52f961c60348ebb109adde9f4635d7540
Provides: bundled(golang(github.com/fsnotify/fsnotify)) = 7d7316ed6e1ed2de075aab8dfc76de5d158d66e1
Provides: bundled(golang(github.com/fsouza/go-dockerclient)) = master
Provides: bundled(golang(github.com/ghodss/yaml)) = 04f313413ffd65ce25f2541bfd2b2ceec5c0908c
Provides: bundled(golang(github.com/godbus/dbus)) = a389bdde4dd695d414e47b755e95e72b7826432c
Provides: bundled(golang(github.com/gogo/protobuf)) = c0656edd0d9eab7c66d1eb0c568f9039345796f7
Provides: bundled(golang(github.com/golang/glog)) = 23def4e6c14b4da8ac2ed8007337bc5eb5007998
Provides: bundled(golang(github.com/golang/groupcache)) = b710c8433bd175204919eb38776e944233235d03
Provides: bundled(golang(github.com/golang/protobuf)) = 4bd1920723d7b7c925de087aa32e2187708897f7
Provides: bundled(golang(github.com/googleapis/gnostic)) = 0c5108395e2debce0d731cf0287ddf7242066aba
Provides: bundled(golang(github.com/google/gofuzz)) = 44d81051d367757e1c7c6a5a86423ece9afcf63c
Provides: bundled(golang(github.com/gorilla/context)) = v1.1
Provides: bundled(golang(github.com/gorilla/mux)) = v1.3.0
Provides: bundled(golang(github.com/hashicorp/errwrap)) = 7554cd9344cec97297fa6649b055a8c98c2a1e55
Provides: bundled(golang(github.com/hashicorp/golang-lru)) = 0a025b7e63adc15a622f29b0b2c4c3848243bbf6
Provides: bundled(golang(github.com/hashicorp/go-multierror)) = 83588e72410abfbe4df460eeb6f30841ae47d4c4
Provides: bundled(golang(github.com/imdario/mergo)) = 0.2.2
Provides: bundled(golang(github.com/json-iterator/go)) = 1.0.0
Provides: bundled(golang(github.com/kr/pty)) = v1.0.0
Provides: bundled(golang(github.com/mattn/go-runewidth)) = v0.0.1
Provides: bundled(golang(github.com/Microsoft/go-winio)) = 78439966b38d69bf38227fbf57ac8a6fee70f69a
Provides: bundled(golang(github.com/Microsoft/hcsshim)) = 43f9725307998e09f2e3816c2c0c36dc98f0c982
Provides: bundled(golang(github.com/mistifyio/go-zfs)) = v2.1.1
Provides: bundled(golang(github.com/mrunalp/fileutils)) = master
Provides: bundled(golang(github.com/mtrmac/gpgme)) = b2432428689ca58c2b8e8dea9449d3295cf96fc9
Provides: bundled(golang(github.com/Nvveen/Gotty)) = master
Provides: bundled(golang(github.com/opencontainers/go-digest)) = v1.0.0-rc0
Provides: bundled(golang(github.com/opencontainers/image-spec)) = v1.0.0
Provides: bundled(golang(github.com/opencontainers/runc)) = b4e2ecb452d9ee4381137cc0a7e6715b96bed6de
Provides: bundled(golang(github.com/opencontainers/runtime-spec)) = v1.0.0
Provides: bundled(golang(github.com/opencontainers/runtime-tools)) = 625e2322645b151a7cbb93a8b42920933e72167f
Provides: bundled(golang(github.com/opencontainers/selinux)) = b6fa367ed7f534f9ba25391cc2d467085dbb445a
Provides: bundled(golang(github.com/openshift/imagebuilder)) = master
Provides: bundled(golang(github.com/ostreedev/ostree-go)) = master
Provides: bundled(golang(github.com/pkg/errors)) = v0.8.0
Provides: bundled(golang(github.com/pmezard/go-difflib)) = 792786c7400a136282c1664665ae0a8db921c6c2
Provides: bundled(golang(github.com/pquerna/ffjson)) = d49c2bc1aa135aad0c6f4fc2056623ec78f5d5ac
Provides: bundled(golang(github.com/projectatomic/buildah)) = 35a37f36d37bf84397d7f79f6bb8649f728c19f1
Provides: bundled(golang(github.com/seccomp/containers-golang)) = master
Provides: bundled(golang(github.com/seccomp/libseccomp-golang)) = v0.9.0
Provides: bundled(golang(github.com/sirupsen/logrus)) = v1.0.0
Provides: bundled(golang(github.com/spf13/pflag)) = 9ff6c6923cfffbcd502984b8e0c80539a94968b7
Provides: bundled(golang(github.com/stretchr/testify)) = 4d4bfba8f1d1027c4fdbe371823030df51419987
Provides: bundled(golang(github.com/syndtr/gocapability)) = e7cb7fa329f456b3855136a2642b197bad7366ba
Provides: bundled(golang(github.com/tchap/go-patricia)) = v2.2.6
Provides: bundled(golang(github.com/ulikunitz/xz)) = v0.5.4
Provides: bundled(golang(github.com/ulule/deepcopier)) = master
# version can't have '-'
#Provides: bundled(golang(github.com/urfave/cli)) = fix-short-opts-parsing
Provides: bundled(golang(github.com/varlink/go)) = master
Provides: bundled(golang(github.com/vbatts/tar-split)) = v0.10.2
Provides: bundled(golang(github.com/vishvananda/netlink)) = master
Provides: bundled(golang(github.com/vishvananda/netns)) = master
Provides: bundled(golang(github.com/xeipuuv/gojsonpointer)) = master
Provides: bundled(golang(github.com/xeipuuv/gojsonreference)) = master
Provides: bundled(golang(github.com/xeipuuv/gojsonschema)) = master
Provides: bundled(golang(golang.org/x/crypto)) = 81e90905daefcd6fd217b62423c0908922eadb30
Provides: bundled(golang(golang.org/x/net)) = c427ad74c6d7a814201695e9ffde0c5d400a7674
Provides: bundled(golang(golang.org/x/sys)) = master
Provides: bundled(golang(golang.org/x/text)) = f72d8390a633d5dfb0cc84043294db9f6c935756
Provides: bundled(golang(golang.org/x/time)) = f51c12702a4d776e4c1fa9b0fabab841babae631
Provides: bundled(golang(google.golang.org/grpc)) = v1.0.4
Provides: bundled(golang(gopkg.in/cheggaaa/pb.v1)) = v1.0.7
Provides: bundled(golang(gopkg.in/inf.v0)) = v0.9.0
Provides: bundled(golang(gopkg.in/mgo.v2)) = v2
Provides: bundled(golang(gopkg.in/square/go-jose.v2)) = v2.1.3
Provides: bundled(golang(gopkg.in/yaml.v2)) = v2
Provides: bundled(golang(k8s.io/api)) = 5ce4aa0bf2f097f6021127b3d879eeda82026be8
Provides: bundled(golang(k8s.io/apiextensions-apiserver)) = 1b31e26d82f1ec2e945c560790e98f34bb5f2e63
Provides: bundled(golang(k8s.io/apimachinery)) = 616b23029fa3dc3e0ccefd47963f5651a6543d94
Provides: bundled(golang(k8s.io/apiserver)) = 4d1163080139f1f9094baf8a3a6099e85e1867f6
Provides: bundled(golang(k8s.io/client-go)) = 7cd1d3291b7d9b1e2d54d4b69eb65995eaf8888e
Provides: bundled(golang(k8s.io/kube-openapi)) = 275e2ce91dec4c05a4094a7b1daee5560b555ac9
Provides: bundled(golang(k8s.io/utils)) = 258e2a2fa64568210fbd6267cf1d8fd87c3cb86e

%description
%{summary}
libpod provides a library for applications looking to use
the Container Pod concept popularized by Kubernetes.

%package docker
Summary: Emulate Docker CLI using podman
BuildArch: noarch
Requires: %{name} = %{version}-%{release}
Conflicts: docker
Provides : docker
Conflicts: docker-latest
Conflicts: docker-ce
Conflicts: docker-ee

%description docker
This package installs a script named docker that emulates the Docker CLI by
executing %{name} commands, it also creates links between all Docker CLI man
pages and %{name}.

%prep
%autosetup -Sgit -n %{repo}-%{commit}
mv pkg/hooks/README.md pkg/hooks/README-hooks.md

# untar cri-o
tar zxf %{SOURCE1}

%build
mkdir -p $(pwd)/_build
pushd $(pwd)/_build
mkdir -p src/%{provider}.%{provider_tld}/%{project}
ln -s ../../../../ src/%{import_path}
popd
ln -s vendor src

export GOPATH=$(pwd):$(pwd)/_build
export BUILDTAGS="selinux seccomp exclude_graphdriver_btrfs exclude_graphdriver_devicemapper $(hack/libdm_tag.sh)"
%gobuild -o bin/%{name} %{import_path}/cmd/%{name}

# build conmon
pushd cri-o-%{commit_conmon}
mkdir _output
pushd _output
mkdir -p src/%{provider}.%{provider_tld}/{kubernetes-sigs,opencontainers}
ln -s $(dirs +1 -l) src/%{import_path_conmon}
popd

ln -s vendor src
export GOPATH=$(pwd):$(pwd)/_output
export BUILDTAGS="selinux seccomp exclude_graphdriver_btrfs exclude_graphdriver_devicemapper $(hack/libdm_tag.sh)"
%gobuild -o bin/crio-config %{import_path_conmon}/cmd/crio-config
cd conmon && ../bin/crio-config
%{__make} all
popd

%install
install -dp %{buildroot}{%{_unitdir}
%{__make} PREFIX=%{buildroot}%{_usr} ETCDIR=%{buildroot}%{_sysconfdir} \
        install.bin \
        install.man \
        install.cni \
        install.systemd \
        install.completions \
        install.docker

# install libpod.conf
install -dp %{buildroot}%{_datadir}/containers
install -p -m 644 %{repo}.conf %{buildroot}%{_datadir}/containers

# install conmon
install -dp %{buildroot}%{_libexecdir}/%{name}
install -p -m 755 cri-o-%{commit_conmon}/bin/conmon %{buildroot}%{_libexecdir}/%{name}

%check
%if 0%{?with_check}
ln -s ./ ./vendor/src # ./vendor/src -> ./vendor
export GOPATH=%{buildroot}/%{gopath}:$(pwd)/vendor:%{gopath}

%gotest %{import_path}/cmd/%{name}
%gotest %{import_path}/libkpod
%gotest %{import_path}/libpod
%gotest %{import_path}/pkg/registrar
%endif

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%files
%license LICENSE
%doc README.md CONTRIBUTING.md pkg/hooks/README-hooks.md install.md code-of-conduct.md transfer.md
%{_bindir}/%{name}
%{_mandir}/man1/podman*.1*
%{_mandir}/man5/*.5*
%{_datadir}/bash-completion/completions/*
%{_libexecdir}/%{name}/conmon
%config(noreplace) %{_sysconfdir}/cni/net.d/87-%{name}-bridge.conflist
%{_datadir}/containers/%{repo}.conf
%{_unitdir}/io.%{name}.service
%{_unitdir}/io.%{name}.socket
%{_usr}/lib/tmpfiles.d/%{name}.conf

%files docker
%{_bindir}/docker
%{_mandir}/man1/docker*.1*

%changelog
* Mon Mar 01 2021 Jindrich Novy <jnovy@redhat.com> - 1.0.0-8.git921f98f
- fix "podman can not create user inside of container" regression introduced by
  patch for CVE-2021-20188
- Related: #1918285

* Fri Feb 12 2021 Jindrich Novy <jnovy@redhat.com> - 1.0.0-7.git921f98f
- fix CVE-2021-20188
- Resolves: #1918285

* Thu Sep 24 2020 Jindrich Novy <jnovy@redhat.com> - 1.0.0-6.git921f98f
- fix "podman run errors out/segfaults in container-tools-1.0-8.3.0"
- Resolves: #1882267

* Fri Jun 26 2020 Jindrich Novy <jnovy@redhat.com> - 1.0.0-5.git921f98f
- bump release to preserve upgrade path
- Resolves: #1821193

* Fri Apr 03 2020 Jindrich Novy <jnovy@redhat.com> - 1.0.0-4.git921f98f
- fix "CVE-2020-10696 buildah: crafted input tar file may lead to local file overwriting during image build process"
- Resolves: #1818122

* Thu Nov 28 2019 Jindrich Novy <jnovy@redhat.com> - 1.0.0-3.git921f98f
- rebuild because of CVE-2019-9512 and CVE-2019-9514
- Resolves: #1766294, #1766322

* Mon Feb 11 2019 Frantisek Kluknavsky <fkluknav@redhat.com> - 1.0.0-2.git921f98f
- rebase

* Tue Jan 15 2019 Frantisek Kluknavsky <fkluknav@redhat.com> - 1.0.0-1.git82e8011
- rebase to v1, yay!
- rebase conmon to 9b1f0a08285a7f74b21cc9b6bfd98a48905a7ba2
- Resolves:#1623282
- python interface removed, moved to https://github.com/containers/python-podman/

* Tue Dec 18 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.12.1.2-4.git9551f6b
- re-enable debuginfo

* Mon Dec 17 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.12.1.2-3.git9551f6b
- python libraries added
- resolves: #1657180

* Mon Dec 17 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.12.1.2-2.git9551f6b
- rebase

* Mon Dec 17 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.11.1.1-3.git594495d
- go tools not in scl anymore

* Mon Nov 19 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.11.1.1-2.git594495d
- fedora-like buildrequires go toolset

* Sat Nov 17 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.11.1.1-1.git594495d
- Resolves: #1636230 - build with FIPS enabled golang toolchain
- bump to v0.11.1.1
- built commit 594495d

* Fri Nov 16 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.11.1-3.gita4adfe5
- podman-docker provides docker
- Resolves: #1650355

* Thu Nov 15 2018 Lumír Balhar <lbalhar@redhat.com> - 0.11.1-2.gita4adfe5
- Require platform-python-setuptools instead of python3-setuptools
- Resolves: rhbz#1650144

* Tue Nov 13 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.11.1-1.gita4adfe5
- bump to v0.11.1
- built libpod commit a4adfe5
- built conmon from cri-o commit 464dba6

* Fri Oct 19 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.10.1.3-5.gitdb08685
- Resolves: #1625384 - keep BR: device-mapper-devel but don't build with it
- not having device-mapper-devel seems to have brew not recognize %%{_unitdir}

* Thu Oct 18 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.10.1.3-4.gitdb08685
- Resolves: #1625384 - correctly add buildtags to remove devmapper

* Thu Oct 18 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.10.1.3-3.gitdb08685
- Resolves: #1625384 - build without device-mapper-devel (no podman support) and lvm2

* Wed Oct 17 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.10.1.3-2.gitdb08685
- Resolves: #1625384 - depend on lvm2

* Wed Oct 17 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.10.1.3-1.gitdb08685
- Resolves: #1640298 - update vendored buildah to allow building when there are
running containers
- bump to v0.10.1.3
- built podman commit db08685

* Wed Oct 17 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.10.1.2-1.git2b4f8d1
- Resolves: #1625378
- bump to v0.10.1.2
- built podman commit 2b4f8d1

* Tue Oct 16 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.10.1.1-1.git4bea3e9
- bump to v0.10.1.1
- built podman commit 4bea3e9

* Thu Oct 11 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.10.1-1.gite4a1553
- bump podman to v0.10.1
- built podman commit e4a1553
- built conmon from cri-o commit a30f93c

* Tue Oct 09 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.9.3.1-4.git1cd906d
- rebased cri-o to 1.11.6

* Wed Sep 26 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.9.3.1-3.git1cd906d
- rebase

* Tue Sep 18 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.9.2-2.git37a2afe
- rebase to podman 0.9.2
- rebase to cri-o 0.11.4

* Tue Sep 11 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.9.1.1-2.git123de30
- rebase

* Mon Aug 27 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.8.4-1.git9f9b8cf
- bump to v0.8.4
- built commit 9f9b8cf
- upstream username changed from projectatomic to containers
- use containernetworking-plugins >= 0.7.3-5

* Mon Aug 13 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.8.2.1-2.git7a526bb
- Resolves: #1615607 - rebuild with gobuild tag 'no_openssl'

* Sun Aug 12 2018 Dan Walsh <dwalsh@redhat.com> - 0.8.2.1-1.git7a526bb
- Upstream 0.8.2.1 release
- Add support for podman-docker
Resolves: rhbz#1615104

* Fri Aug 10 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.8.2-1.dev.git8b2d38e
- Resolves: #1614710 - podman search name includes registry
- bump to v0.8.2-dev
- built libpod commit 8b2d38e
- built conmon from cri-o commit acc0ee7

* Wed Aug 8 2018 Dan Walsh <dwalsh@redhat.com> - 0.8.1-2.git6b4ab2a
- Add recommends for slirp4netns and container-selinux

* Tue Aug 07 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.8.1-2.git6b4ab2a
- bump to v0.8.1
- use %%go{build,generate} instead of go build and go generate
- update go deps to use scl-ized builds
- No need for Makefile patch for python installs

* Sat Aug 4 2018 Dan Walsh <dwalsh@redhat.com> - 0.8.1-1.git6b4ab2a
- Bump to v0.8.1

* Wed Aug 1 2018 Dan Walsh <dwalsh@redhat.com> - 0.7.4-2.git079121
- podman should not require atomic-registries

* Tue Jul 24 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.7.4-1.dev.git9a18681
- bump to v0.7.4-dev
- built commit 9a18681

* Sat Jul 21 2018 Dan Walsh <dwalsh@redhat.com> - 0.7.3-2.git079121
- Turn on ostree support
- Upstream 0.7.3

* Sat Jul 14 2018 Dan Walsh <dwalsh@redhat.com> - 0.7.2-2.git4ca4c5f
- Upstream 0.7.2 release

* Wed Jul 11 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.7.1-3.git84cfdb2
- rebuilt

* Wed Jul 11 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.7.1-2.git84cfdb2
- rebase to 84cfdb2

* Sun Jul 08 2018 Dan Walsh <dwalsh@redhat.com> - 0.7.1-1.git802d4f2
- Upstream 0.7.1 release

* Mon Jun 25 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.6.4-2.gitd5beb2f
- disable devel and unittest subpackages
- include conditionals for rhel-8.0

* Fri Jun 22 2018 Dan Walsh <dwalsh@redhat.com> - 0.6.4-1.gitd5beb2f
- do not compress debuginfo with dwz to support delve debugger

* Mon Jun 04 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.6.1-3.git3e0ff12
- do not compress debuginfo with dwz to support delve debugger

* Mon Jun 04 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.6.1-2.git3e0ff12
- bash completion shouldn't have shebang

* Mon Jun 04 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.6.1-1.git3e0ff12
- Resolves: #1584429 - drop capabilities when running a container as non-root
- bump to v0.6.1
- built podman commit 3e0ff12
- built conmon from cri-o commit 1c0c3b0
- drop containernetworking-plugins subpackage, it's now split out into a standalone
package

* Fri Apr 27 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.4.1-4.gitb51d327
- Resolves: #1572538 - build host-device and portmap plugins

* Thu Apr 12 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.4.1-3.gitb51d327
- correct dep on containernetworking-plugins

* Thu Apr 12 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.4.1-2.gitb51d327
- add containernetworking-plugins v0.7.0 as a subpackage (podman dep)
- release tag for the containernetworking-plugins is actually gotten from
podman release tag.

* Wed Apr 11 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.4.1-1.gitb51d327
- bump to v0.4.1
- built commit b51d327

* Wed Mar 14 2018 Lokesh Mandvekar <lsm5@fedoraproject.org> - 0.3.3-1.dev.gitbc358eb
- built podman commit bc358eb
- built conmon from cri-o commit 712f3b8

* Fri Mar 09 2018 baude <bbaude@redhat.com> - 0.3.2-1.gitf79a39a
- Release 0.3.2-1

* Sun Mar 04 2018 baude <bbaude@redhat.com> - 0.3.1-2.git98b95ff
- Correct RPM version

* Fri Mar 02 2018 baude <bbaude@redhat.com> - 0.3.1-1-gitc187538
- Release 0.3.1-1

* Sun Feb 25 2018 Peter Robinson <pbrobinson@fedoraproject.org> 0.2.2-2.git525e3b1
- Build on ARMv7 too (Fedora supports containers on that arch too)

* Fri Feb 23 2018 baude <bbaude@redhat.com> - 0.2.2-1.git525e3b1
- Release 0.2.2

* Fri Feb 16 2018 baude <bbaude@redhat.com> - 0.2.1-1.git3d0100b
- Release 0.2.1

* Wed Feb 14 2018 baude <bbaude@redhat.com> - 0.2-3.git3d0100b
- Add dep for atomic-registries

* Tue Feb 13 2018 baude <bbaude@redhat.com> - 0.2-2.git3d0100b
- Add more 64bit arches
- Add containernetworking-cni dependancy
- Add iptables dependancy

* Mon Feb 12 2018 baude <bbaude@redhat.com> - 0-2.1.git3d0100
- Release 0.2

* Tue Feb 06 2018 Lokesh Mandvekar <lsm5@fedoraproject.org> - 0-0.3.git367213a
- Resolves: #1541554 - first official build
- built commit 367213a

* Fri Feb 02 2018 Lokesh Mandvekar <lsm5@fedoraproject.org> - 0-0.2.git0387f69
- built commit 0387f69

* Wed Jan 10 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 0-0.1.gitc1b2278
- First package for Fedora

